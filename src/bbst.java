

import java.io.*;
import java.util.StringTokenizer;
/**
*   This class represents a single node in the tree. Each node has Event ID
*   and it count as members.
*
*/
class TreeNode{
    public static TreeNode Tnil = new TreeNode(); // This is the nil node
    public int eventId, count;
    public TreeNode leftChild;
    public TreeNode rightChild;
    public TreeNode parent;
    public boolean colorRed = true; // By default any new node will have the color red set to true
    /**
    *   Constructor for TreeNode
    *   @param eventId The integer representing the eventId
     *  @param count   The count for the given event ID
    */
    public TreeNode(int eventId, int count){
        this.eventId = eventId;
        this.count = count;
        colorRed = true;
        //All the pointers point to the nil node.
        leftChild = Tnil;
        rightChild = Tnil;
        parent = Tnil;
    }

    /**
    *   Overloaded Constructor for TreeNode to build the nil node
    *   This constructor does not accept any argument
    */
    public TreeNode(){
        eventId = 0;
        count = 0;
        colorRed =  false;//Nil nodes are always black.
    }
    /**
    *   This method increases the count of the Event objects that's calling it
     *   by the value given in byValue.
     *
     *   @param byValue     Integer value by which the count should be raised.
     */
    public int increaseNodeCount(int byValue){
        this.count += byValue;
        return this.count;
    }

    /**
    *   This method decreases the count of the Event objects that's calling it
     *   by the value given in byValue.
     *
     *   @param byValue     Integer value by which the count should be decreased.
     */
    public int decreaseNodeCount(int byValue){
        this.count-=byValue;
        return this.count;
    }


}

/**
* This class represents the Nodes in the tree.
* The class has reference to the root of the tree in the form of treeRoot.
 */
class RBTree{
    public TreeNode treeRoot; //root of the tree
    public int count = 0;
    /**
    *   Constructor to initialize the tree. Sets the tree node to nil node.
    */
    public RBTree() {
        treeRoot = TreeNode.Tnil;
    }

    /**
     * This search is similar to a search in a BST.
     * @param pEventID  Pass the event ID to be searched here.
     * @return TreeNode that contains the found node. Its Tnil if the node was not found.
     */
    public TreeNode standardSearch(int pEventID){
        TreeNode tempNode = this.treeRoot;
        while(tempNode != TreeNode.Tnil && tempNode.eventId != pEventID){
            if(pEventID <= tempNode.eventId)
                tempNode = tempNode.leftChild;
            else
                tempNode = tempNode.rightChild;
        }
        return tempNode;
    }

    /**
     * This routine inserts a new node in the RBTree and then calls fixup to
     * fix any rules that were violated.
     * @param newTreeNode Pass the new TreeNode that is to be inserted.
     */
    public void insertRBTree(TreeNode newTreeNode){
        TreeNode temp = TreeNode.Tnil;
        TreeNode start = this.treeRoot;
        //Look for the node
        while(start!=TreeNode.Tnil){
            temp = start;
            if(newTreeNode.eventId<start.eventId){
                start = start.leftChild;
            }else{
                start = start.rightChild;
            }
        }
        //insert new node
        newTreeNode.parent = temp;
        if(temp==TreeNode.Tnil){
            this.treeRoot = newTreeNode;
        }else if(newTreeNode.eventId < temp.eventId){
            temp.leftChild = newTreeNode;
        }else{
            temp.rightChild = newTreeNode;
        }
        newTreeNode.leftChild = TreeNode.Tnil;
        newTreeNode.rightChild = TreeNode.Tnil;
        newTreeNode.colorRed = true; //new nodes have color red by default
        RBFixTree(treeRoot, newTreeNode);//call to fixup any rules that were violated.
    }

    /**
     * This routine deletes the given node from the RB Tree
     * @param deleteNode    Pass the TreeNode that is to be deleted.
     * @return boolean  This is currently set to true.
     */
    public boolean deleteRBTree(TreeNode deleteNode){
        TreeNode x = TreeNode.Tnil;
        TreeNode tempNode = deleteNode;
        boolean tempOriginalColor = tempNode.colorRed;
        if(deleteNode.leftChild==TreeNode.Tnil){
            x = deleteNode.rightChild;
            this.transplantNode(deleteNode,deleteNode.rightChild);

        }else if(deleteNode.rightChild==TreeNode.Tnil){
            x = deleteNode.leftChild;
            this.transplantNode(deleteNode, deleteNode.leftChild);
        }else{
            tempNode = this.treeMinimum(deleteNode.rightChild);
            tempOriginalColor = tempNode.colorRed;
            x = tempNode.rightChild;
            if(tempNode.parent==deleteNode){
                x.parent = tempNode;
            }else{
                this.transplantNode(tempNode,tempNode.rightChild);
                tempNode.rightChild = deleteNode.rightChild;
                tempNode.rightChild.parent = tempNode;
            }
            this.transplantNode(deleteNode,tempNode);
            tempNode.leftChild = deleteNode.leftChild;
            tempNode.leftChild.parent = tempNode;
            tempNode.colorRed = deleteNode.colorRed;
        }
        if(!tempOriginalColor){
            fixDeleteRB(x);
        }
        return true;
    }

    /**
     * Fix the tree after delete if any rules were violated.
     * @param fixNode Pass the node that replaced the original node.
     */
    private void fixDeleteRB(TreeNode fixNode){
        TreeNode siblingNode = TreeNode.Tnil;
        while(fixNode!=this.treeRoot && fixNode.colorRed == false){
            if(fixNode == fixNode.parent.leftChild){
                siblingNode = fixNode.parent.rightChild;
                if(siblingNode.colorRed==true){
                    siblingNode.colorRed = false;
                    fixNode.parent.colorRed = true;
                    leftRotate(fixNode.parent);
                    siblingNode = fixNode.parent.rightChild;
                }
                if(siblingNode.leftChild.colorRed==false && siblingNode.rightChild.colorRed==false){
                    siblingNode.colorRed = true;
                    fixNode = fixNode.parent;
                }else{
                    if(siblingNode.rightChild.colorRed==false){
                        siblingNode.leftChild.colorRed = false;
                        siblingNode.colorRed = true;
                        rightRotate(siblingNode);
                        siblingNode = fixNode.parent.rightChild;
                    }
                    siblingNode.colorRed = fixNode.parent.colorRed;
                    fixNode.parent.colorRed = false;
                    siblingNode.rightChild.colorRed = false;
                    leftRotate(fixNode.parent);
                    fixNode = this.treeRoot;
                }
            }else{
                siblingNode = fixNode.parent.leftChild;
                if(siblingNode.colorRed==true){
                    siblingNode.colorRed = false;
                    fixNode.parent.colorRed = true;
                    rightRotate(fixNode.parent);
                    siblingNode = fixNode.parent.leftChild;
                }
                if(siblingNode.leftChild.colorRed==false && siblingNode.rightChild.colorRed==false){
                    siblingNode.colorRed = true;
                    fixNode = fixNode.parent;
                }else{
                    if(siblingNode.leftChild.colorRed==false){
                        siblingNode.rightChild.colorRed = false;
                        siblingNode.colorRed = true;
                        leftRotate(siblingNode);
                        siblingNode = fixNode.parent.leftChild;
                    }
                    siblingNode.colorRed = fixNode.parent.colorRed;
                    fixNode.parent.colorRed = false;
                    siblingNode.leftChild.colorRed = false;
                    rightRotate(fixNode.parent);
                    fixNode = this.treeRoot;
                }
            }
        }
        fixNode.colorRed = false;
    }

    /**
     * Fix the tree after an insert.
     * @param root      This is the root of the tree
     * @param newNodeZ  This is the newly inserted node.
     */
    private void RBFixTree(TreeNode root, TreeNode newNodeZ){
        while(newNodeZ.parent.colorRed){
            if(newNodeZ.parent.parent.leftChild == newNodeZ.parent){
                TreeNode y = newNodeZ.parent.parent.rightChild;
                if(y.colorRed){
                    newNodeZ.parent.colorRed = false;
                    y.colorRed = false;
                    newNodeZ.parent.parent.colorRed = true;
                    newNodeZ = newNodeZ.parent.parent;
                }else {
                    if (newNodeZ == newNodeZ.parent.rightChild) {
                        newNodeZ = newNodeZ.parent;
                        leftRotate(newNodeZ);
                    }
                    newNodeZ.parent.colorRed = false;
                    newNodeZ.parent.parent.colorRed = true;
                    rightRotate(newNodeZ.parent.parent);
                }
            }
            else{
                TreeNode y = newNodeZ.parent.parent.leftChild;
                if(y.colorRed){
                    newNodeZ.parent.colorRed = false;
                    y.colorRed = false;
                    newNodeZ.parent.parent.colorRed = true;
                    newNodeZ = newNodeZ.parent.parent;
                }else {
                    if (newNodeZ == newNodeZ.parent.leftChild) {
                        newNodeZ = newNodeZ.parent;
                        rightRotate(newNodeZ);
                    }
                    newNodeZ.parent.colorRed = false;
                    newNodeZ.parent.parent.colorRed = true;
                    leftRotate(newNodeZ.parent.parent);
                }
            }

        }
        this.treeRoot.colorRed = false;
    }

    /**
     * Left rotate the sub tree rooted at pivot
     * @param pivot The node which is the pivot for the roation.
     */
    private void leftRotate(TreeNode pivot){
        TreeNode y = pivot.rightChild;
        pivot.rightChild = y.leftChild;
        if(y.leftChild!=TreeNode.Tnil){
            y.leftChild.parent = pivot;
        }
        y.parent = pivot.parent;
        if(pivot.parent==TreeNode.Tnil){
            this.treeRoot = y;
        }else if(pivot.parent.leftChild == pivot){
            pivot.parent.leftChild = y;
        }else if(pivot.parent.rightChild == pivot){
            pivot.parent.rightChild = y;
        }
        y.leftChild = pivot;
        pivot.parent = y;
    }

    /**
     * Right Rotate the sub tree rooted at pivot
     * @param pivot The node which is the pivot for the rotation
     */
    private void rightRotate(TreeNode pivot){
        TreeNode x = pivot.leftChild;
        pivot.leftChild = x.rightChild;
        if(x.rightChild!=TreeNode.Tnil)
            x.rightChild.parent = pivot;

        x.parent = pivot.parent;

        if(pivot.parent==TreeNode.Tnil){
            this.treeRoot = x;
        }else if(pivot.parent.leftChild == pivot){
            pivot.parent.leftChild = x;
        }else if(pivot.parent.rightChild == pivot){
            pivot.parent.rightChild = x;
        }

        x.rightChild = pivot;
        pivot.parent = x;
    }

    /**
     * Find the minimum value in the tree rooted at the start node.
     * @param startNode The start node of the subtree
     * @return The node which has the minimum value.
     */
    private TreeNode treeMinimum(TreeNode startNode){
        TreeNode temp = startNode;
        while(temp.leftChild!=TreeNode.Tnil){
            temp = temp.leftChild;
        }
        return temp;
    }

    /**
     * Find the maximum value in the tree rooted at the start node.
     * @param startNode The start node of the subtree
     * @return The node which has the maximum value.
     */
    private TreeNode treeMaximum(TreeNode startNode){
        TreeNode temp = startNode;
        while(temp.rightChild!=TreeNode.Tnil){
            temp=temp.rightChild;
        }
        return temp;
    }

    /**
     * Transplant the original node with the new node.
     * @param originalNode The original node
     * @param newNode The new node.
     */
    private void transplantNode(TreeNode originalNode, TreeNode newNode){
        if(originalNode.parent== TreeNode.Tnil){
            this.treeRoot = newNode;
        }else if(originalNode.parent.leftChild == originalNode){
            originalNode.parent.leftChild = newNode;
        }else if(originalNode.parent.rightChild == originalNode){
            originalNode.parent.rightChild = newNode;
        }
        newNode.parent = originalNode.parent;
    }

    /**
     * This routine calculates the sum of all the counts between the lower
     * and the higher event id and sets it in count.
     * @param root  The root of the tree/subtree under consideration
     * @param lowerValue The lowerValue for the range
     * @param higherValue The higher value of the range
     */
    public void inRange(TreeNode root, int lowerValue, int higherValue){
        if(root.leftChild != TreeNode.Tnil&&
                root.eventId > lowerValue){
            inRange(root.leftChild,lowerValue, higherValue);

        }

        if(root.eventId >= lowerValue &&
                root.eventId <= higherValue){
            count += root.count;
        }

        if(root.rightChild!=TreeNode.Tnil &&
                root.eventId < higherValue){
            inRange(root.rightChild, lowerValue, higherValue);
        }
    }

    /**
     * Though not part of the project, this routine prints the inorder traversal.
     * For use in testing
     * @param root Root of the tree
     * @param i The level count
     * @param pw The printwriter to write to a file.
     */
    public void inOrder(TreeNode root,int i, PrintWriter pw){
        if(root==TreeNode.Tnil){
            return;
        }

       for(int j=0;j<i;j++)
           // pw.print(".");
            System.out.print(".");

       // pw.println(root.eventId+"("+((root.colorRed)?"r":"b")+")");
        System.out.println(root.eventId+"("+((root.colorRed)?"r":"b")+")");

        inOrder(root.leftChild,i+1,pw);

        inOrder(root.rightChild,i+1,pw);

    }

    /**
     * Get the successor of the node.
     * @param nodeInput The start node.
     * @return The successor node. Tnil, if no successor
     */
    public TreeNode getSuccessor(TreeNode nodeInput){
        if(nodeInput.rightChild!=TreeNode.Tnil){
            return this.treeMinimum(nodeInput.rightChild);
        }
        TreeNode tempNode = nodeInput.parent;
        while(tempNode!=TreeNode.Tnil && nodeInput == tempNode.rightChild){
            nodeInput = tempNode;
            tempNode = tempNode.parent;
        }

        return tempNode;
    }

    /**
     * Get the predecessor of the node.
     * @param nodeInput The node whole predecessor is to be found.
     * @return The predecessor node. Tnil if the node is not found.
     */
    public TreeNode getPredecessor(TreeNode nodeInput){
        if(nodeInput.leftChild!=TreeNode.Tnil){
            return this.treeMaximum(nodeInput.leftChild);
        }

        TreeNode tempNode = nodeInput.parent;
        while(tempNode!=TreeNode.Tnil && nodeInput == tempNode.leftChild){
            nodeInput = tempNode;
            tempNode = tempNode.parent;
        }
        return tempNode;
    }
}

/**
 * This class will load the file and build the tree.
 *
 */
class FileLoader{


    String fileName; // Input file name

    InputStream is = null; //Input stream for file
    BufferedReader br; //Buffered reader for file
    double height; //This will store the height of the tree.

    /**
     * Initialize the file readers.
     */
    private void initScanner(){
        try {
            is = new BufferedInputStream(new FileInputStream(fileName),8*2*1024*1024); // set my own buffer size for faster reads
            br = new BufferedReader(new InputStreamReader(is));
        }catch (FileNotFoundException e){
            System.out.println("Error opening file");//File not found will get us here.
            System.exit(-1);
        }
    }

    /**
     * Get Redblack Tree Data from the file.
     * @return A two dimensional array with dimensions nx2 with the eventId and their counts.
     */
    private int[][] getTreeDataFromFile(){

        int[][] data = null;
        try {
            String line = br.readLine(); //read a line. This will get us value of n
            StringTokenizer st = new StringTokenizer(line," "); //Tokens based on space
            int n = Integer.parseInt(st.nextToken());
            this.height = (Math.log(n)/Math.log(2));//Only leaf nodes at this level are marked red. This ensures
                                                    //that the number of black nodes in each path remain the same.
            data = new int[n][2];                   //New array to hold the data. We read the data at once so that
                                                    //we can build the array recursively in O(N)

            int locationCounter = 0; //counter for data store.
            while((line = br.readLine())!=null){
                st = new StringTokenizer(line," ");
                while(st.hasMoreTokens()){

                    data[locationCounter][0] = Integer.parseInt(st.nextToken());//eventId
                    data[locationCounter][1] = Integer.parseInt(st.nextToken());//count
                    locationCounter++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }

    /**
     * Constructor: inits the file name
     * @param fileName  The filename to be read
     */
    public FileLoader(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Load file in memory and get the tree
     * @return RBTree Handler Class Object
     */
    public RBTree loadFile(){
        initScanner(); //init the readers

        int[][] data = getTreeDataFromFile();//Read the data from the file
	
        return buildTree(data); //Build tree with data and return

    }

    /**
     * This routines builds the actual tree
     * @param data  The 2D array that contains sorted eventIds and their counts.
     * @return RBTree
     */
    public RBTree buildTree(int[][] data){
        RBTree localRBTreeHandler = new RBTree();
        localRBTreeHandler.treeRoot = getTree(data,0,data.length-1,1);

        return localRBTreeHandler;
    }

    /**
     * Helps with tree generation. This is basically inorder building of tree.
     * @param data  2D array with sorted eventId and data
     * @param start Start index of array
     * @param end   End index of array
     * @param count Keeps record of the level.
     * @return Root of the newly build tree
     */
    public TreeNode getTree(int[][] data, int start, int end, int count){
        if(start>end){
            return TreeNode.Tnil;//Nil if start>end
        }
        int mid = (start + end)/2; //find the mid point
	
        TreeNode newNode = new TreeNode(data[mid][0],data[mid][1]); //build root of subtree
        newNode.leftChild = getTree(data,start,mid-1,count+1); // Get left child recursively
        newNode.rightChild = getTree(data,mid+1,end,count+1); //Get right child recursively

        // Color the nodes appropriately
        if(newNode.leftChild!=TreeNode.Tnil){
            newNode.leftChild.parent = newNode;
            newNode.colorRed = false;
        }
        if(newNode.rightChild!=TreeNode.Tnil){
            newNode.rightChild.parent = newNode;
            newNode.colorRed = false;
        }
        if(newNode.colorRed&&count!=((int)height+1)){
            
            newNode.colorRed = false;
        }
        return newNode;
    }
}

/**
 * This class acts as an interface between the commands and the tree handler.
 */
class CommandExecutor {

    RBTree localRBTreeObject; // The local Tree Handler reference

    /**
     * Constructor
     * @param fileName  FileName to be read
     */
    public CommandExecutor(String fileName){
        localRBTreeObject = new FileLoader(fileName).loadFile(); //Get Tree with the help of FileLoader

    }

    /**
     * This will increase the key of the node that matches eventId and print it.
     * If the node does not exist, this will create the new node.
     * @param eventId eventId
     * @param count   increase by value
     */
    public void increaseKey(int eventId, int count){
        TreeNode tempNode = localRBTreeObject.standardSearch(eventId);
        if(tempNode==TreeNode.Tnil){
            localRBTreeObject.insertRBTree(new TreeNode(eventId,count));
            System.out.println(count);
        }else{
            System.out.println(tempNode.increaseNodeCount(count));
        }
    }

    /**
     * This will decrease the key of the node that matches eventId and print it.
     * If the node does not exist, this will print 0 0
     * @param eventId eventId
     * @param count   decrease by value
     */
    public void decreaseKey(int eventId, int count){
        TreeNode tempNode = localRBTreeObject.standardSearch(eventId);
        if(tempNode==TreeNode.Tnil){
            System.out.println(0);
        }else{
            int newValue = tempNode.decreaseNodeCount(count);
            if(newValue<1){
                localRBTreeObject.deleteRBTree(tempNode);
                System.out.println(0);
            }else{
                System.out.println(newValue);
            }
        }
    }

    /**
     * This will print the count of the node which matches eventid
     * @param eventId The event id whose count is to be printed.
     */
    public void getCount(int eventId){
        TreeNode temp = localRBTreeObject.standardSearch(eventId);
        if(temp==TreeNode.Tnil){
            System.out.println(0);

        }else{
            System.out.println(temp.count);
        }
    }

    /**
     * This will sum up the count of all records in range.
     * @param eventId1 Lower value of the range
     * @param eventId2 Higher value of the range.
     */
    public void inRange(int eventId1, int eventId2){
        localRBTreeObject.count = 0;
        localRBTreeObject.inRange(localRBTreeObject.treeRoot,eventId1,eventId2);
        System.out.println(localRBTreeObject.count);
    }

    /**
     * This will print the node next to the eventid and its count
     * @param eventId Event Id
     */
    public void next(int eventId){
        TreeNode target = findNext(localRBTreeObject.treeRoot,eventId);
        System.out.println(target.eventId + " " + target.count);
    }

    /**
     * Helper function to find the next value
     * @param startRoot Start Node
     * @param eventId EventId
     * @return The result node. Nil on case nothing found
     */
    private TreeNode findNext(TreeNode startRoot, int eventId){
        if(startRoot == TreeNode.Tnil){
            return TreeNode.Tnil;
        }
        if(startRoot.eventId==eventId){
            return(localRBTreeObject.getSuccessor(startRoot));
        }
        if(startRoot.eventId<eventId){
            return findNext(startRoot.rightChild,eventId);
        }
        TreeNode foundNode = findNext(startRoot.leftChild,eventId);
        return ((foundNode!=TreeNode.Tnil&&foundNode.eventId>=eventId)?foundNode:startRoot);
    }

    /**
     * This will print the node which is previous to the given event Id
     * @param eventId Event ID
     */
    public void previous(int eventId){
        TreeNode target = findPrevious(localRBTreeObject.treeRoot,eventId);
        System.out.println(target.eventId + " " + target.count);
    }

    /**
     * Helper function to find the previous value
     * @param startNode Start Node
     * @param eventId The event Id
     * @return The previous node
     */
    private TreeNode findPrevious(TreeNode startNode, int eventId){
        if(startNode==TreeNode.Tnil){
            return startNode;
        }
        if(startNode.eventId==eventId){
            return(localRBTreeObject.getPredecessor(startNode));
        }

        if(startNode.eventId>eventId){
            return findPrevious(startNode.leftChild,eventId);
        }

        TreeNode foundNode = findPrevious(startNode.rightChild,eventId);
        return ((foundNode!=TreeNode.Tnil&&foundNode.eventId<=eventId)?foundNode:startNode);
    }

}

public class bbst {
    /**
     * Main Method
     * @param args Arguments should contain the file name
     */
    public static void main(String[] args) {
        if(args.length<1){
            System.out.println("Please enter an input file name");
            System.exit(-1); //Error if no file name supplied
        }

        CommandExecutor localCommandExecuter = new CommandExecutor(args[0]); //used to execute commands
        BufferedReader localBr = new BufferedReader(new InputStreamReader(System.in));//read input from stdin.
                                                                                        //BufferedReader used to improve performance
        while(true){

            try{
                String line[] = localBr.readLine().split(" ");

                switch (line[0]){
                    case "increase":

                        localCommandExecuter.increaseKey(Integer.parseInt(line[1]),Integer.parseInt(line[2]));
                        break;
                    case "reduce":

                        localCommandExecuter.decreaseKey(Integer.parseInt(line[1]),Integer.parseInt(line[2]));
                        break;
                    case "count":

                        localCommandExecuter.getCount(Integer.parseInt(line[1]));
                        break;
                    case "next":

                        localCommandExecuter.next(Integer.parseInt(line[1]));
                        break;
                    case "previous":

                        localCommandExecuter.previous(Integer.parseInt(line[1]));
                        break;
                    case "inrange":

                        localCommandExecuter.inRange(Integer.parseInt(line[1]),Integer.parseInt(line[2]));
                        break;
                    case "quit":

                        System.exit(0);
                        break;
                    default: System.out.println("Invalid Command");
                }

            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }



    }
}
